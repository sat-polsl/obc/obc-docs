# On-board Computer - Documentation

## [OBC Functional Requirements](https://gitlab.com/sat-polsl/obc/obc-doc/-/wikis/OBC)

## Hardware chart

![hw-chart](./hardware/hw-chart.png "Hardware Chart")

## Used Hardware

|Type|Part|Interface|Amount|
|---|---|---|:---:|
|MCU|STM32F446||1|
|||**Serial**||
|GPS|uBlox NEO-M8N|Serial|2|
|RTC|uBlox NEO-M8N|Serial|2|
|||**Analog**||
|Thermometer|PT100|Analog|1|
|||**SPI**||
|RTD to Digital Converter|MAX31865|SPI|1|
|uSD Card Reader||SPI|1|
|||**I2C**||
|uSD Card||SPI|1|
|FRAM||I2C|1|
|Accelerometer and Gyroscope|LSM6DS33|I2C|1|
|Magnetometer|LIS3MDL|I2C|1|

## Software flowchart

![flowchart](./software/flowchart.png "Flowchart")

- uSD logger,
- IMU Readout,
- RTC and GPS Readout,
- Thermometer Readout,
- CAN communication,
- Interrupt.